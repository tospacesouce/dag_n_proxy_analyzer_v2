package main

import (
	"dag_analyzer/workdir"
	"fmt"
	"github.com/fatih/color"
	"log"
	"os"
	"path/filepath"
	"time"
)

const (
	Greeting         = "Choose path to assets directory. \nYou can type it or drag and drop it here"
	outputFolderName = "_fix"
	blkFolder        = "_temp-blk"
)

func main() {
	appMsg := color.New(color.FgGreen, color.Bold).SprintFunc()
	note := color.New(color.FgYellow, color.Bold).SprintFunc()
	fmt.Println(appMsg(Greeting))

	sourceDir := ""

	fmt.Scanln(&sourceDir)

	for {
		fmt.Println(appMsg("Choose path for INFO files.\n")+
			appMsg("Press "), note("Enter"), appMsg(" for use "), note("analyse"), appMsg(" path.\n")+
			appMsg("Or type "), note("src"), appMsg(" for use "), note("assets path.\n")+
			appMsg("Type or drop "), note("custom path"), appMsg(", for use your own."))

		infoPath := ""

		fmt.Scanln(&infoPath)

		var err error = nil

		switch infoPath {
		case "":
			workingDir := filepath.Dir(os.Args[0])
			if _, err = filepath.Abs(workingDir); err != nil {
				log.Fatal(err)
			}

			err = workdir.PrepareDirectories(workingDir, outputFolderName)

			err = workdir.PrepareDirectories(workingDir, blkFolder)

		case "src":
			err = workdir.PrepareDirectories(sourceDir, outputFolderName)

			err = workdir.PrepareDirectories(sourceDir, blkFolder)

		default:
			err = workdir.PrepareDirectories(infoPath, outputFolderName)

			err = workdir.PrepareDirectories(infoPath, blkFolder)
		}

		if err == nil {
			break
		}
	}

	fmt.Println("Folder created")

	time.Sleep(time.Minute / 10)
}
