package workdir

import (
	"fmt"
	"os"
	"path/filepath"
)

func PrepareDirectories(path, dir string) error {
	if err := clearOldDir(path, dir); err != nil {
		return err
	}

	if err := createNewDir(path, dir); err != nil {
		return err
	}

	return nil
}

func clearOldDir(path, dir string) error {
	if _, err := os.Stat(filepath.Join(path, dir)); !os.IsNotExist(err) {
		err = removeContents(filepath.Join(path, dir))
		if err != nil {
			fmt.Printf("cannot delete folder \"%v\" due error: %v\n", dir, err)

			return err
		}
	}

	return nil
}

func createNewDir(path, dir string) error {
	if err := os.Mkdir(filepath.Join(path, dir), 0755); err != nil {
		fmt.Printf("cannot create folder \"%v\" due error: %v\n", dir, err)

		return err
	}

	return nil
}

func removeContents(dir string) error {
	if err := os.RemoveAll(dir); err != nil {
		return err
	}

	return nil
}
